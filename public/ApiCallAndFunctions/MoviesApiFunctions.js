import axios from "axios";

const fetch_movies_genres = async (
  baseUrl,
  path_for_genreList,
  API_KEY,
  language,
  movie_Categories
) => {
  try {
    /// genre url fetch
    const genreURL = new URL(path_for_genreList, baseUrl);
    genreURL.searchParams.set("api_key", API_KEY);
    genreURL.searchParams.set("language", language);

    const genre_res = await axios.get(genreURL);

    let { data } = genre_res;
    const { genres } = data;

    const trueCategoryIds = [];

    for (const category in movie_Categories) {
      if (movie_Categories[category]) {
        const matchingCategory = genres.find(
          (genre) => genre.name === category
        );
        if (matchingCategory) {
          trueCategoryIds.push(matchingCategory);
        }
      }
    }

    return trueCategoryIds;
  } catch (error) {
    return error;
  }
};

const fetch_movies_list = async (
  baseUrl,
  path_for_movieList,
  API_KEY,
  language,
  genreId
) => {
  try {
    const url = new URL(path_for_movieList, baseUrl);

    url.searchParams.set("api_key", API_KEY);
    url.searchParams.set("language", language);
    url.searchParams.set("page", "1");
    url.searchParams.set("with_genres", genreId);

    const response = await axios.get(url);

    const { data } = response;

    const { results } = data;

    return results;
  } catch (error) {
    return error;
  }
};

export { fetch_movies_genres, fetch_movies_list };
