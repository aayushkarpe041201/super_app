import axios from "axios";

const dummy_data = [
  {
    source: {},
    author: "EditorDavid",
    title:
      "Perl 5.38 Released with New Experimental Syntax for Defining Object Classes",
    description: `Perl 5.38 was released this week "after being in dn 'field' variables that behave like lexicals." `,
    url: "https://developers.slashdot.org/story/23/07/08/005w-experimental-syntax-for-defining-object-classes",
  },
  {
    source: {},
    author: "Diwas Haldar",
    title: "9 Ways to Improve Your Online Presence in 2023",
    description:
      "Establishing a robust online presence is imperativine Presence in 2023 appeared first on ReadWrite.",
    url: "https://readwrite.com/ways-to-improve-your-online-presence-in-2023/",
  },
  {
    source: {},
    author: "Peggy Paul Casella",
    title: "How to Actually Find Good Recipes Online",
    description:
      "So many food blogs and recipe sites are samey and le to find something you'll love to cook and eat.",
    url: "https://www.wired.com/story/how-to-find-good-recipes-online/",
  },
  {
    author: "Peggy Paul Casella",
    content:
      "Ever been catfished by a recipe that ranks at the top of your Google search but turns out meh when you make it yourself? Google does mean well by favoring longer, keyword-rich posts, videos, and stud… [+4556 chars]",
    description:
      "So many food blogs and recipe sites are samey and full of pretty pictures but untested recipes. Don't worry, it's possible to find something you'll love to cook and eat.",
    publishedAt: "2023-07-02T11:00:00Z",
    source: { id: "wired", name: "Wired" },
    title: "How to Actually Find Good Recipes Online",
    url: "https://www.wired.com/story/how-to-find-good-recipes-online/",
    urlToImage:
      "https://media.wired.com/photos/649ed317da92561daff93bbf/191:100/w_1280,c_limit/recipe_digital_box_culture_GettyImages-1302884718.jpg",
  },
  {
    source: {},
    author: "Prashant Kumar",
    title:
      "How Artificial Intelligence is Revolutionizing Search Engine Optimization (SEO)",
    description:
      "Artificial Intelligence search engine OptimizationEngine Optimization (SEO) appeared first on Read",
    url: "https://readwrite.com/how-artificial-intelligence-is-revolutionizing-search-engine-optimization-seo/",
  },
  {
    source: {},
    author: "Zach Edelstein",
    title: "6 Ways ChatGPT Can Improve Your SEO",
    description:
      "From on-page optimizations to technical SEO, AI ca traffic with these tips from Mozs SEO Director.",
    url: "https://moz.com/blog/ai-tools-to-improve-seo",
  },
];

const fetch_News_data = async (baseUrl, path, API_KEY, q) => {
  const url = new URL(path, baseUrl);

  url.searchParams.set("apiKey", API_KEY);
  url.searchParams.set("q", q);

  try {
    const res = await axios.get(url);

    const { data } = res;
    if (data.hasOwnProperty("status") && data.status === "ok") {
      const { articles } = data;
      return articles;
    } else {
      return dummy_data;
    }
  } catch (error) {
    return dummy_data;
  }
};

export default fetch_News_data;
