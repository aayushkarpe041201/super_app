import axios from "axios";

const fetch_data = async (baseUrl, path, API_KEY, location) => {
  const url = new URL(path, baseUrl);

  url.searchParams.set("key", API_KEY);
  url.searchParams.set("q", location);

  try {
    const res = await axios.get(url);
    return res;
  } catch (error) {
    return error;
  }
};

export default fetch_data;
