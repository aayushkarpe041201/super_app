import { BrowserRouter, Route, Routes } from "react-router-dom";

import appCss from "../css/App.module.css";
import SignUpPage from "../screens/SignUpPage";
import CategoryPage from "../screens/CategoryPage";
import NotesPage from "../screens/NotesPage";
import BrowsePage from "../screens/BrowsePage";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path='/'
          element={<SignUpPage />}
        />
        <Route
          path='/Category'
          element={<CategoryPage />}
        />
        <Route
          path='/notes'
          element={<NotesPage />}
        />
        <Route
          path='/Browser'
          element={<BrowsePage />}
        />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
