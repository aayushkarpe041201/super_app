import { useState } from "react";
import cc from "../css/categoryCard.module.css";

function CategoryCard({ txt, imgUrl, bgCol, borderCol, change_val }) {
  const handleChange = () => {
    change_val(txt, true);
  };
  return (
    <div
      onClick={handleChange}
      style={{ backgroundColor: bgCol, border: `0.15rem solid ${borderCol}` }}
      id={cc.categorybox}>
      <p>{txt}</p>
      <img
        src={imgUrl}
        alt={"img"}
      />
    </div>
  );
}

export default CategoryCard;
