import { useEffect, useState } from "react";
import { fetch_movies_list } from "../../public/ApiCallAndFunctions/MoviesApiFunctions";

import lop from "../css/ListOfMovies.module.css";

function ListOfMovies({ genre }) {
  const baseUrl = "https://api.themoviedb.org/3/";

  const path_for_movieList = "movie/upcoming";

  const API_KEY = "e20002a6d8685e27f00627af4c4b5567";

  const language = "en-US";

  const [movieData, setmovieData] = useState(null);

  useEffect(() => {
    const fetch_movie_data = async () => {
      try {
        const get_list = await fetch_movies_list(
          baseUrl,
          path_for_movieList,
          API_KEY,
          language,
          genre.id
        );

        console.log(genre.id);
        setmovieData(get_list);
      } catch (error) {
        console.log(error);
      }
    };

    fetch_movie_data();
  }, []);

  return (
    <>
      {movieData ? (
        <div id={lop.lopMainContainer}>
          <p id={lop.lopMainContainerP}>{genre.name}</p>

          <div id={lop.lopMovieImgs}>
            {movieData[0] ? (
              <img
                src={`https://image.tmdb.org/t/p/original${movieData[0].backdrop_path}`}
                alt='img'
              />
            ) : (
              <></>
            )}
            {movieData[1] ? (
              <img
                src={`https://image.tmdb.org/t/p/original${movieData[1].backdrop_path}`}
                alt='img'
              />
            ) : (
              <></>
            )}
            {movieData[2] ? (
              <img
                src={`https://image.tmdb.org/t/p/original${movieData[2].backdrop_path}`}
                alt='img'
              />
            ) : (
              <></>
            )}
            {movieData[3] ? (
              <img
                src={`https://image.tmdb.org/t/p/original${movieData[3].backdrop_path}`}
                alt='img'
              />
            ) : (
              <></>
            )}
          </div>
        </div>
      ) : (
        <p>Loding... </p>
      )}
    </>
  );
}

export default ListOfMovies;
