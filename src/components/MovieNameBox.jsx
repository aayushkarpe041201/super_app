import { useEffect } from "react";
import mb from "../css/movieNameBox.module.css";

function MovieNameBox({ Mname, change_val, bgCol, crossVisible }) {
  const makeChanges = () => {
    change_val(Mname, false);
  };

  const handleButtonClick = (e) => {
    e.stopPropagation();
    makeChanges();
  };
  return (
    <>
      {Mname ? (
        <div
          id={mb.movieBoxMain}
          style={{ backgroundColor: bgCol }}>
          <p id={mb.movitxt}>{Mname}</p>{" "}
          <button
            style={{ display: crossVisible }}
            id={mb.closeButton}
            onClick={handleButtonClick}>
            &times;
          </button>
        </div>
      ) : (
        ""
      )}
    </>
  );
}

export default MovieNameBox;
