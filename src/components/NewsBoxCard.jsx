import { useEffect, useState } from "react";
import nbc from "../css/newsBoxCard.module.css";
import fetch_News_data from "../../public/ApiCallAndFunctions/NewsApiFunction";

const baseUrl = "https://newsapi.org";
const path = "/v2/everything";

const q = "keyword";
const API_KEY = "e6059659e585467f9a8c940dbf5272eb";

function NewsBoxCard() {
  const [newsData, setnewsData] = useState(null);

  useEffect(() => {
    const fetchNewsData = async () => {
      try {
        const response = await fetch_News_data(baseUrl, path, API_KEY, q);

        setnewsData(response);
        console.log(response[3])
        console.log(response);
      } catch (error) {
        console.log(error);
      }
    };
    fetchNewsData();
  }, []);
  return (
    <>
      {newsData ? (
        <div id={nbc.nbcMainContainer}>
          <div id={nbc.nbcimgAndHeading}>
            <img
              src={newsData[3]?.urlToImage ?? ""}
              alt='img'
            />
            <div id={nbc.nbcHeading}>
              <h3>{newsData[3]?.title ?? ""}</h3>
              <p>
                {new Date(newsData[3]?.publishedAt ?? "")
                  .toLocaleString("en-US", {
                    year: "numeric",
                    month: "2-digit",
                    day: "2-digit",
                    hour: "numeric",
                    minute: "numeric",
                  })
                  .replace(",", " |")
                  .replace(/\//g, "-")}
              </p>
            </div>
          </div>
          <div id={nbc.nbcNews}>{newsData[3]?.description ?? ""}</div>
        </div>
      ) : (
        <p>Loding...</p>
      )}
    </>
  );
}

export default NewsBoxCard;
