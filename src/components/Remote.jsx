import { useEffect, useState } from "react";
import r from "../css/Remote.module.css";
import SetBtn from "./SetBtn";

function Remote({ timeFun, letsStart, clear }) {
  const [setTime, setsetTime] = useState({
    Hours: 0,
    Minutes: 0,
    Seconds: 0,

    totalSec() {
      return this.Hours * 3600 + this.Minutes * 60 + this.Seconds;
    },
  });

  const onArrowCLick = (val, name) => {
    setdataClear(false)
    setsetTime({ ...setTime, [name]: val });
  };

  useEffect(() => {
    timeFun(setTime.totalSec());
  }, [setTime]);

  
  const [dataClear, setdataClear] = useState(false);
  
  const handleStart = () => {
    letsStart();
    setdataClear(true);
  };

  useEffect(() => {
    if (clear) {
      setsetTime({
        Hours: 0,
        Minutes: 0,
        Seconds: 0,

        totalSec() {
          return this.Hours * 3600 + this.Minutes * 60 + this.Seconds;
        },
      });
    }
  }, [clear]);

  return (
    <div
      id={r.rMainContainer}
      className={r.center}>
      <div
        id={r.rbtnContainer}
        className={r.center}>
        <SetBtn
          id={r.rHr}
          className={r.btnClass}
          name={"Hours"}
          onArrowCLick={onArrowCLick}
          clear={dataClear}
        />
        <SetBtn
          id={r.rMin}
          className={r.btnClass}
          name={"Minutes"}
          onArrowCLick={onArrowCLick}
          clear={dataClear}
        />
        <SetBtn
          id={r.rSec}
          className={r.btnClass}
          name={"Seconds"}
          onArrowCLick={onArrowCLick}
          clear={dataClear}
        />
      </div>

      <button
        id={r.rbtn}
        className={r.center}
        onClick={handleStart}>
        Start
      </button>
    </div>
  );
}

export default Remote;
