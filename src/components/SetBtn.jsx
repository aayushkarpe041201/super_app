import React, { useEffect, useState } from "react";

function SetBtn({ name, onArrowCLick, clear }) {
  const [val, setval] = useState(0);

  useEffect(() => {
    onArrowCLick(val, name);
  }, [val]);

  useEffect(() => {
    // if(clear){
    //   let value = 0;
    //   setval(value)
    // }
  }, [clear]);
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "inherit",
        flexDirection: "column",
        gap: "1rem",
      }}>
      <p
        style={{
          color: "#949494",
          fontFamily: "Roboto",
          fontSize: "1.2rem",
          fontStyle: "normal",
          fontWeight: "400",
          lineHeight: "27.479px",
          letterSpacing: "1.04px",
          backgroundColor: "inherit",
        }}>
        {name}
      </p>

      <svg
        xmlns='http://www.w3.org/2000/svg'
        width='27'
        height='18'
        viewBox='0 0 27 18'
        fill='none'
        style={{ backgroundColor: "inherit", cursor: "pointer" }}
        onClick={() => setval((prevData) => prevData + 1)}>
        <path
          d='M10.8779 1.12301L1.14451 10.8564C-0.0409617 12.0418 -0.305509 13.3983 0.350868 14.9256C1.00724 16.453 2.17649 17.218 3.85862 17.2205H23.1381C24.8227 17.2205 25.9932 16.4555 26.6496 14.9256C27.306 13.3958 27.0402 12.0393 25.8522 10.8564L16.1189 1.12301C15.7445 0.74865 15.339 0.467882 14.9022 0.280703C14.4655 0.093523 13.9975 -7.05719e-05 13.4984 -7.05719e-05C12.9992 -7.05719e-05 12.5313 0.093523 12.0945 0.280703C11.6578 0.467882 11.2522 0.74865 10.8779 1.12301Z'
          fill='#949494'
        />
      </svg>

      <p
        style={{
          color: "#FFFD",
          fontFamily: "Roboto",
          fontSize: "2rem",
          fontStyle: "normal",
          fontWeight: "300",
          lineHeight: "52.844px",
          letterSpacing: "2px",
          backgroundColor: "inherit",
        }}>
        {val.toLocaleString("en-US", { minimumIntegerDigits: 2 })}
      </p>

      <svg
        xmlns='http://www.w3.org/2000/svg'
        width='27'
        height='18'
        viewBox='0 0 27 18'
        fill='none'
        style={{ backgroundColor: "inherit", cursor: "pointer" }}
        onClick={() =>
          setval((prevData) => (prevData <= 0 ? 0 : prevData - 1))
        }>
        <path
          d='M10.8779 16.0975L1.14451 6.36411C-0.0409617 5.17864 -0.305509 3.82221 0.350868 2.29482C1.00724 0.767436 2.17649 0.00249573 3.85862 0H23.1381C24.8227 0 25.9932 0.764941 26.6496 2.29482C27.306 3.8247 27.0402 5.18113 25.8522 6.36411L16.1189 16.0975C15.7445 16.4718 15.339 16.7526 14.9022 16.9398C14.4655 17.1269 13.9975 17.2205 13.4984 17.2205C12.9992 17.2205 12.5313 17.1269 12.0945 16.9398C11.6578 16.7526 11.2522 16.4718 10.8779 16.0975Z'
          fill='#949494'
        />
      </svg>
    </div>
  );
}

export default SetBtn;
