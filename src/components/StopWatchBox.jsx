import { useState } from "react";
import swb from "../css/StopWatchBox.module.css";
import Remote from "./Remote";
import Watch from "./Watch";

function StopWatchBox() {
  const [time, settime] = useState(0);
  const [startVal, setstartVal] = useState(false);
  const [clear, setClear] = useState(false);

  const timeFun = (timeInSec) => {
    settime(timeInSec);
  };

  const letsStart = () => {
    setstartVal(true);
  };

  const clearFun = () => {
    console.log("clear Fun")
    setClear(true);
    setstartVal(false);
  };

  return (
    <div id={swb.swbMianCotainer}>
      <Watch
        id={swb.swbClock}
        totTime={time}
        start={startVal}
        clearFun={clearFun}
      />
      <Remote
        id={swb.swbSetTimerRemote}
        timeFun={timeFun}
        letsStart={letsStart}
        clear={clear}
      />
    </div>
  );
}

export default StopWatchBox;
