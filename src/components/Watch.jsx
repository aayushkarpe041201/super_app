import React, { useEffect, useRef, useState } from "react";
import w from "../css/Watch.module.css";

function Watch({ totTime, start, clearFun }) {
  const [val, setval] = useState(false);
  const [tempVal, setTempVal] = useState(false);

  const [time, setTime] = useState("00:00:00");

  let totalSeconds = totTime - 1; // if 6 then it will have 7 sec
  let totalSeconds_ = totTime - 1; // if 6 then it will have 7 sec

  useEffect(() => {
    if (start) {
      setval(true);
    } else {
      setval(false);
    }
  }, [start]);

  useEffect(() => {
    if (val) {
      const interval = setInterval(() => {
        const hours = Math.floor(totalSeconds / 3600).toLocaleString("en-US", {
          minimumIntegerDigits: 2,
        });
        const minutes = Math.floor((totalSeconds % 3600) / 60).toLocaleString(
          "en-US",
          { minimumIntegerDigits: 2 }
        );
        const seconds = (totalSeconds % 60).toLocaleString("en-US", {
          minimumIntegerDigits: 2,
        });

        setTime(`${hours}:${minutes}:${seconds}`);
        setTempVal(true);

        console.log(totalSeconds);
        totalSeconds -= 1;

        if (totalSeconds <= -1) {
          playAudio();
          clearInterval(interval);
          clearFun();
          setval(false);
          setTempVal(false);
          setTime("00:00:00");
          totalSeconds = totTime - 1;
          totalSeconds_ = totTime - 1;
        }
      }, 1000);
    }
  }, [val]);

  const playAudio = () => {
    const audio = new Audio("\\assets\\timer_3_beeps.mp3");
    audio.play();
  };

  return (
    <div
      id={w.wMainContainer}
      className={w.center}>
      <div className={`${w.circleContainer} ${w.center}`}>
        <div
          className={w.semicircle}
          style={
            tempVal
              ? {
                  transform: "rotate(180deg)",
                  transition: `${totalSeconds_ / 2}s linear`,
                }
              : {}
          }></div>
        <div
          className={w.semicircle}
          style={
            tempVal
              ? {
                  transform: "rotate(360deg)",
                  transition: `${totalSeconds_}s linear`,
                }
              : {}
          }></div>
        <div
          className={w.semicircle}
          style={
            tempVal
              ? {
                  opacity: 0,
                  transition: ` opacity 0s ${totalSeconds_ / 2}s`,
                }
              : {}
          }></div>
        <div className={w.outermostcircle}></div>
      </div>

      <div id={w.wTimer}>{time}</div>

      {/* <button
        style={{ backgroundColor: "pink", color: "red", zIndex: 10 }}
        onClick={handleClick}>
        start
      </button> */}
    </div>
  );
}

export default Watch;
