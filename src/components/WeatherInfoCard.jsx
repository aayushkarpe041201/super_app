import wic from "../css/WeatherInfoCard.module.css";

import fetch_data from "../../public/ApiCallAndFunctions/WeatherApiFunctions";
import { useEffect, useState } from "react";

function WeatherInfoCard() {
  const basUrl = "https://api.weatherapi.com/v1/";
  const path = "current.json";

  const API_KEY = "595665682fb443ea98462102231107";

  const location = "Mumbai";

  const [DNT, setDNT] = useState(null);
  const [weatherInfo, setWeatherInfo] = useState(null);

  useEffect(() => {
    const myData = async () => {
      try {
        const get_data = await fetch_data(basUrl, path, API_KEY, location);

        const { data } = get_data;

        const { condition, gust_kph, humidity, pressure_mb, temp_c, wind_kph } =
          data.current;

        setWeatherInfo({
          condition,
          gust_kph,
          humidity,
          pressure_mb,
          temp_c,
          wind_kph,
        });

        const { localtime } = data.location;

        setDNT(localtime);
      } catch (error) {
        console.log(error);
      }
    };

    myData();
  }, []);

  const getTimeFun = (timeString) => {
    const timeString12hr = new Date(
      "1970-01-01T" + timeString + "Z"
    ).toLocaleTimeString("en-US", {
      timeZone: "UTC",
      hour12: true,
      hour: "numeric",
      minute: "numeric",
    });

    return timeString12hr;
  };

  return (
    <>
      {weatherInfo && (
        <div id={wic.wicMainContainer}>
          <div id={wic.wicMainContainerTND}>
            <p id={wic.wicDate}>{DNT.split(" ")[0]}</p>
            <p id={wic.wicTime}>{getTimeFun(DNT.split(" ")[1])}</p>
          </div>
          <div id={wic.wicMainContainerWeatherInfo}>
            <div id={wic.wicWeatherInfoImg}>
              <img
                src={weatherInfo.condition.icon}
                alt='img'
              />
            </div>
            <hr />
            <div id={wic.wicWeatherInfoTempNPressure}>
              <p id={wic.wicWeatherInfoTempNPressureDegree}>
                {weatherInfo.temp_c} &deg;C
              </p>

              <div
                id={wic.wicWeatherInfoTempNPressureConatainer}
                style={{
                  display: "flex",
                  backgroundColor: "inherit",
                  justifyContent: "center",
                  alignItems: "center",
                  gap: "1rem",
                }}>
                <img
                  src={"assets\\Vector.png"}
                  alt='img'
                />
                <p id={wic.wicWeatherInfoTempNPressurePressure}>
                  {weatherInfo.pressure_mb} mbar Pressure
                </p>
              </div>
            </div>
            <hr />
            <div id={wic.wicWeatherInfoWindNHumidityContainer}>
              <div id={wic.wicWeatherInfoWind}>
                <img
                  src={"assets\\Vector (1).png"}
                  alt='img'
                />
                <p>{weatherInfo.wind_kph} km/h wind</p>
              </div>
              <div id={wic.wicWeatherInfoHymidity}>
                <img
                  src={"assets\\Group.png"}
                  alt='img'
                />
                <p>{weatherInfo.humidity}% Humidity</p>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default WeatherInfoCard;
