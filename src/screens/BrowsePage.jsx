import bp from "../css/BrowserPage.module.css";
import { useEffect, useState } from "react";

import { fetch_movies_genres } from "../../public/ApiCallAndFunctions/MoviesApiFunctions";

import ListOfMovies from "../components/ListOfMovies";
import { useNavigate } from "react-router-dom";

function BrowsePage() {
  const navigate = useNavigate();

  const [UserData, setUserData] = useState(
    JSON.parse(localStorage.getItem("userData"))
  );
  const [genres, setgenres] = useState(null);

  const baseUrl = "https://api.themoviedb.org/3/";

  const path_for_genreList = "genre/movie/list";

  const API_KEY = "e20002a6d8685e27f00627af4c4b5567";

  const language = "en-US";

  useEffect(() => {
    const fetch_genres = async () => {
      try {
        const get_genres = await fetch_movies_genres(
          baseUrl,
          path_for_genreList,
          API_KEY,
          language,
          UserData.chosenCategory
        );

        setgenres(get_genres);
      } catch (error) {
        console.error(error);
      }
    };
    fetch_genres();
  }, []);

  const handleBtnNavigateToCategoryPage = () => {
    navigate("/Category");
  };

  return (
    <div id={bp.bpMainConatiner}>
      <header id={bp.bpHeadingContainer}>
        <h1>Super App</h1>
        <img
          src={"assets\\image 14.png"}
          alt=''
        />
      </header>

      <div id={bp.bpMoviesContainer}>
        <p id={bp.bpMoviesContainerP}>Entertainment according to your choice</p>
        {genres ? (
          <>
            {genres.map((genre) => (
              <ListOfMovies
                genre={genre}
                key={genre.id}
              />
            ))}
          </>
        ) : (
          <div>Loading...</div>
        )}
      </div>

      <button
        id={bp.bpBtnDirectToCP}
        onClick={handleBtnNavigateToCategoryPage}>
        Category Page
      </button>
    </div>
  );
}

export default BrowsePage;
