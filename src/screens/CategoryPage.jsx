import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

import cp from "../css/categoryPage.module.css";
import CategoryCard from "../components/CategoryCard";
import MovieNameBox from "../components/MovieNameBox";

function CategoryPage() {
  const [category, setcategory] = useState({
    Action: false,
    Drama: false,
    Romance: false,
    Thriller: false,
    Western: false,
    Horror: false,
    Fantasy: false,
    Music: false,
    Fiction: false,
  });

  const navigate = useNavigate();

  const [min3, setmin3] = useState(0);
  const [nextPageClicked, setNextPageClicked] = useState(false);

  const handleNextPageClick = () => {
    setNextPageClicked(true);

    const selectedCategories = Object.values(category).filter(
      (category) => category
    );

    if (selectedCategories.length > 2) {
      const userData = localStorage.getItem("userData");
      const parsedData = JSON.parse(userData);
      const finalData = { ...parsedData, chosenCategory: category };

      localStorage.setItem("userData", JSON.stringify(finalData));

      navigate("/notes");
    } else {
      setmin3(1);
    }
  };

  const change_val = (key, value) => {
    setcategory((prevData) => ({ ...prevData, [key]: value }));

    const selectedCategories = Object.values(category).filter(
      (category) => category
    );
    if (selectedCategories.length < 2) {
      setmin3(1);
    } else {
      setmin3(0);
    }
  };

  useEffect(() => {
    // Retrieve form data from local storage on component mount
    const loacalStorage_data = localStorage.getItem("userData");
    const loacalStorage_data_parsed = JSON.parse(loacalStorage_data);
    const storedcategory = loacalStorage_data_parsed.chosenCategory;

    if (storedcategory) {
      setcategory(storedcategory);
    }
  }, []);

  useEffect(() => {}, [category]);

  return (
    <div id={cp.main}>
      <div id={cp.leftBox}>
        <h1>Super app</h1>
        <p>Choose your entertainment category</p>
        <div id={cp.addedCatergoryDiv}>
          {Object.keys(category).map((Objkey) => {
            const value = category[Objkey];

            if (value) {
              return (
                <MovieNameBox
                  Mname={Objkey}
                  bgCol={"#148a08"}
                  key={Objkey}
                  change_val={change_val}
                  crossVisible={"block"}
                />
              );
            }
          })}
        </div>
        <p
          id={cp.errorp}
          style={{ display: nextPageClicked && min3 ? "block" : "none" }}>
          Minimum 3 category required
        </p>
      </div>
      <div id={cp.rightBox}>
        <div id={cp.gridbox}>
          <CategoryCard
            txt={"Action"}
            imgUrl={"assets\\image 2.png"}
            bgCol={"#FF5209"}
            borderCol={"#11B800"}
            change_val={change_val}
          />
          <CategoryCard
            txt={"Drama"}
            imgUrl={"assets\\image 3.png"}
            bgCol={"#D7A4FF"}
            change_val={change_val}
          />
          <CategoryCard
            txt={"Romance"}
            imgUrl={"assets\\image 4.png"}
            bgCol={"#148A08"}
            borderCol={"#11B800"}
            change_val={change_val}
          />
          <CategoryCard
            txt={"Thriller"}
            imgUrl={"assets\\image 6.png"}
            bgCol={"#84C2FF"}
            change_val={change_val}
          />
          <CategoryCard
            txt={"Western"}
            imgUrl={"assets\\image 7.png"}
            bgCol={"#902500"}
            change_val={change_val}
          />
          <CategoryCard
            txt={"Horror"}
            imgUrl={"assets\\image 8.png"}
            bgCol={"#7358FF"}
            change_val={change_val}
          />
          <CategoryCard
            txt={"Fantasy"}
            imgUrl={"assets\\image 9.png"}
            bgCol={"#FF4ADE"}
            change_val={change_val}
          />
          <CategoryCard
            txt={"Music"}
            imgUrl={"assets\\image 10.png"}
            bgCol={"#E61E32"}
            borderCol={"#11B800"}
            change_val={change_val}
          />
          <CategoryCard
            txt={"Fiction"}
            imgUrl={"assets\\image 11.png"}
            bgCol={"#6CD061"}
            change_val={change_val}
          />
        </div>
        <button
          id={cp.btn}
          onClick={handleNextPageClick}>
          Next Page
        </button>
      </div>
    </div>
  );
}

export default CategoryPage;
