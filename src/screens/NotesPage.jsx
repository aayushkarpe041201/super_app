import { useEffect, useState } from "react";

import NewsBoxCard from "../components/NewsBoxCard";
import WeatherInfoCard from "../components/WeatherInfoCard";
import np from "../css/NotesPage.module.css";
import { useNavigate } from "react-router-dom";
import StopWatchBox from "../components/StopWatchBox";

function NotesPage() {
  const navigate = useNavigate();

  const [UserData, setUserData] = useState({});
  const [categoryData, setcategoryData] = useState({});

  const [NotePadText, setNotePadText] = useState(
    JSON.parse(localStorage.getItem("userData")).notes
  );

  useEffect(() => {
    const localData = localStorage.getItem("userData");

    const data = JSON.parse(localData);

    const category_data = data.chosenCategory;
    const notes_data = data.notes;
    if (data) {
      setUserData(data);
    }
    if (category_data) {
      setcategoryData(category_data);
    }
    if (notes_data) {
      setNotePadText(notes_data);
    }
  }, []);

  // Auto save

  useEffect(() => {
    const local_data = localStorage.getItem("userData");
    const parsedData = JSON.parse(local_data);

    const final_data = { ...parsedData, notes: NotePadText };
    localStorage.setItem("userData", JSON.stringify(final_data));

    return () => {};
  }, [NotePadText]);

  const handleOnChangeNodePadText = (e) => {
    const txt = e.target.value;
    setNotePadText(txt);
  };

  const handleBrowse = () => {
    navigate("/Browser");
  };

  return (
    <>
      <div id={np.NpmainBox}>
        <div id={np.infoBox}>
          <img
            src={"assets\\image 15.png"}
            alt='img'
            id={np.infoboxImg}
          />
          <div id={np.infoBoxDataBox}>
            <p id={np.UserDataName}>{UserData.name}</p>
            <p id={np.UserDataEmail}>{UserData.email}</p>
            <p id={np.UserDatauserName}>{UserData.userName}</p>
          </div>
          <div id={np.infoBoxCategoryBox}>
            {Object.keys(categoryData).map((Objkey) => {
              const value = categoryData[Objkey];

              if (value) {
                return (
                  <div
                    id={np.categoryDataBox}
                    key={Objkey}>
                    {Objkey}
                  </div>
                );
              }
            })}
          </div>
        </div>

        <WeatherInfoCard id={np.weatherInfoBox} />

        <div id={np.notePadBox}>
          {" "}
          <p id={np.InfoBoxHeading}>All notes</p>
          <textarea
            id={np.InfoBoxContent}
            value={NotePadText}
            onChange={handleOnChangeNodePadText}></textarea>
        </div>
        <div id={np.stopWatchBox}>
          <StopWatchBox />
        </div>
        <div id={np.newsBox}>
          <NewsBoxCard />
        </div>

        <button
          id={np.goToBrowseBtn}
          onClick={handleBrowse}>
          Browse
        </button>
      </div>
    </>
  );
}

export default NotesPage;
