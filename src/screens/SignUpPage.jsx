import { useEffect, useState } from "react";
import sup from "../css/signUpPage.module.css";
import { useNavigate } from "react-router-dom";

function SignUpPage() {
  const [formData, setFormData] = useState({
    name: "",
    userName: "",
    email: "",
    phoneNumber: "",
    isCheck: false,
    chosenCategory: {},
    notes: "",
  });

  const [formErrors, setFormErrors] = useState({
    name: "",
    userName: "",
    email: "",
    phoneNumber: "",
    isCheck: "",
  });

  const navigate = useNavigate();

  const handleChange = (e) => {
    const { value, name, type, checked } = e.target;

    setFormData((prevData) => ({
      ...prevData,
      [name]: type === "checkbox" ? checked : value,
    }));
  };

  const validateForm = () => {
    let isValid = true;
    const errors = {};

    if (formData.name.trim() === "") {
      errors.name = "Field is required";
      isValid = false;
    }

    if (formData.userName.trim() === "") {
      errors.userName = "Field is required";
      isValid = false;
    }

    if (formData.email.trim() === "") {
      errors.email = "Field is required";
      isValid = false;
    }

    if (formData.phoneNumber.trim() === "") {
      errors.phoneNumber = "Field is required";
      isValid = false;
    }

    if (!formData.isCheck) {
      errors.isCheck = "Check this box if you want to proceed";
      isValid = false;
    }

    setFormErrors(errors);

    return isValid;
  };

  const submitHandler = (e) => {
    e.preventDefault();

    if (validateForm()) {
      localStorage.setItem("userData", JSON.stringify(formData));

      navigate("/Category");
    }
  };

  useEffect(() => {
    const data = localStorage.getItem("userData");

    if (data) {
      setFormData(JSON.parse(data));
    }
  }, []);

  return (
    <div id={sup.mainBoxsignPage}>
      <div
        id={sup.box1signPage}
        style={{ backgroundImage: "url(assets/image_13.png)" }}>
        <p id={sup.box2signupP}>Discover new things on Superapp</p>
      </div>
      <div id={sup.box2signPage}>
        <h1>Super app</h1>
        <p>Create your new account</p>

        <form onSubmit={submitHandler}>
          <input
            type='text'
            id='inpName'
            name='name'
            placeholder='Name'
            value={formData.name}
            onChange={handleChange}
          />
          {formErrors.name && <p className={sup.errorMsg}>{formErrors.name}</p>}

          <input
            type='text'
            id='inpUserName'
            name='userName'
            placeholder='UserName'
            value={formData.userName}
            onChange={handleChange}
          />
          {formErrors.userName && (
            <p className={sup.errorMsg}>{formErrors.userName}</p>
          )}

          <input
            type='email'
            id='inpEmail'
            name='email'
            placeholder='Email'
            value={formData.email}
            onChange={handleChange}
          />
          {formErrors.email && (
            <p className={sup.errorMsg}>{formErrors.email}</p>
          )}

          <input
            type='tel'
            id='inpMobile'
            name='phoneNumber'
            placeholder='Mobile'
            value={formData.phoneNumber}
            onChange={handleChange}
          />
          {formErrors.phoneNumber && (
            <p className={sup.errorMsg}>{formErrors.phoneNumber}</p>
          )}

          <>
            <input
              type='checkbox'
              id={sup.inpCheckBox}
              name='isCheck'
              value={formData.isCheck}
              onChange={handleChange}
            />
            <label htmlFor={sup.inpCheckBox}>
              Share my registration data with Superapp
            </label>
          </>
          {formErrors.isCheck && (
            <p className={sup.errorMsg}>{formErrors.isCheck}</p>
          )}

          <button
            type='submit'
            id={sup.box1Btnsup}>
            SIGN UP
          </button>
        </form>

        <p className={sup.botTxt}>
          By clicking on Sign up. you agree to Superapp{" "}
          <span>Terms and Conditions of Use</span>
        </p>
        <p className={sup.botTxt}>
          To learn more about how Superapp collects, uses, shares and protects
          your personal data please head Superapp <span>Privacy Policy</span>
        </p>
          
          
      </div>
    </div>
  );
}

export default SignUpPage;
